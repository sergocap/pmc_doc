# Docker ENG

### Installation

`Docker` and `Docker compose` on Ubuntu 17.04 (zesty) x64

##### Docker ([source](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#uninstall-old-versions))

* Configure the repository and install packages
```
apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" # /etc/apt/sources.list
apt update
apt install docker-ce
```
* Add the user `deploy` to the group `docker`
This will allow you to work with `docker` without `sudo`.
> `deploy` - the user on whose behalf the chain will be launched in Ubuntu
```
usermod -aG docker deploy
```

##### Docker compose ([source](https://docs.docker.com/compose/install/#install-compose))

```
curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

# Docker RU

### Установка

`Docker` и `Docker compose` на Ubuntu 17.04 (zesty) x64

##### Docker ([источник](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#uninstall-old-versions))

* Настраиваем репозиторий и ставим пакеты
```
apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" # /etc/apt/sources.list
apt update
apt install docker-ce
```
* Добавить пользователя `deploy` в группу `docker`
Это позволит работать с `docker` без `sudo`.
> `deploy` - пользователь, от лица которого будет проходить запуск цепи в Ubuntu
```
usermod -aG docker deploy
```

##### Docker compose ([источник](https://docs.docker.com/compose/install/#install-compose))

```
curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
