# Invoice ENG

## Interesting fields

* **packages_price_btc** - the sum of the prices of BTC packages
* **packages_power** - sum of the power of TH/s packages
* **reinvoice** (Boolean) - is reinvestment
* **can_price_updating_at** (DateTime) - timestamp when payment can be updated (25 minutes). The limit can be set here `Invoice#try_update_btc_price!`
* **connected** (Boolean) - whether the packet is connected. The package becomes connected and participates in the process of accruing bonuses after the first weekly calculation of `EductionAndReinvoiceWorker`

## Create an investment

* On the `/dashboard/invoices` page, the user selects packages and clicks to invest
* Investment status becomes `created`
* When creating an investment, the package powers are summed, the package prices in USD and BTC - `Invoice#fields_init`
* Then, a `Payment` is created with the new Bitcoin address
* The user replenishes the balance at the address with an arbitrary amount

## Statuses

* **created** - the investment has just been created and is waiting for the first payment
* **less_paid** - underpaid. Occurs when the user has not paid the full investment. When this status occurs, a new Payment and a bitcoin address for payment are created
* **waiting** - when the funds have arrived at the address and this is enough to pay, but waiting for transaction confirmations
* **paid** - when the received funds are equal to the required and confirmations are completed
* **over_paid** - received funds are more than required. The overpayment value is stored in another table `OverpaidReturn` with the status of `ready` - ready for withdrawal.

## update_state!

The method changes the status of the investment depending on `payments`

# Инвестиция RUS

## Интересные поля

* **packages_price_btc** - сумма цен пакетов BTC
* **packages_power** - сумма мощностей пакетов TH/s
* **reinvoice** (Boolean) - является ли реинвестицией
* **can_price_updating_at** (DateTime) - временная метка когда можно обновить payment (25 минут). Лимит можно усановить здесь `Invoice#try_update_btc_price!`
* **connected** (Boolean) - является ли пакет подключённым. Пакет становится подключённым и учавствует в процессе начисления бонусов после первого недельного расчёта `EductionAndReinvoiceWorker`

## Создание инвестиции

* На странице `/dashboard/invoices` пользователь выбирает пакеты и нажимает инвестировать
* Статус инвестиции становится `created`
* При создании инвестии суммируются мощности пакетов, цены пакетов в USD и BTC - `Invoice#fields_init`
* Затем создаётся платёжка (`Payment`) с новым биткойн-адресом
* Пользователь пополняет баланс по адресу произвольной суммой

## Статусы

* **created** - инвестиции только что создана и ждёт первой оплаты
* **less_paid** - недоплачено. Происходит когда пользователь оплатил инвестицию не полностью. При наступлении этого статуса создаёся новый Payment и биткоин-адрес для оплаты
* **waiting** - когда средства поступили на адрес и этого хватает для оплаты, но идёт ожидание подтверждений транзакций
* **paid** - когда поступившие средства равны требуемым и подтверждения завершены
* **over_paid** - поступившие средства больше требуемых. Величина переплаты сохраняется в другой таблице `OverpaidReturn` со статусом `ready` - готово к выводу.

## update_state!

Метод изменяет статус инвестиции в зависимости от `payments`
