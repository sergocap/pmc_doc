# Exchequer (Treasure) ENG

## Admin Panel

![Treasure 1](img/treasure_1.png "Treazure 1")

1. Invested - total working investment
2. Bonus To Pay - bonuses ready for withdrawal (+ overpayments)
3. Power - working purchased power
4. Pool - how many bitcoins were managed to get this week
5. Pool Input - input mined
6. Bonus Paid - withdrawn bonuses for the current week
7. Node - the number of BTC on the wallet
8. Trezor - how much is withdrawn on trezor
9. Node Address - Show the address of the wallet to replenish
10. Display the current week
11. Summary

## Pool

* Shows how much BTC is mined
* `Pool` (`BtcStatistic.now!.mined`) and `Power` (`BtcStatistic.now!.cweek_power`) are involved in the calculation of `IncomeBonus`

# Exchequer (Казначейство) RU

## Admin Panel

![Treasure 1](img/treasure_1.png "Treazure 1")

1. Invested - всего рабочих инвестиций
2. Bonus To Pay - бонусы готовые к выводу (+ переплаты)
3. Power - рабочие купленные мощности
4. Pool - сколько удалось добыть биткоинов на текущей неделе
5. Pool Input - ввод добытого
6. Bonus Paid - выведенные бонусы за текущую неделю
7. Node - количество BTC на кошельке
8. Trezor - сколько выведено на trezor
9. Node Address - Показать адрес кошелька для пополнения
10. Отображение текущей недели
11. Итоги

## Pool

* Показывает сколько добыто BTC
* `Pool` (`BtcStatistic.now!.mined`) и `Power` (`BtcStatistic.now!.cweek_power`) участвуют в начислении `IncomeBonus`
