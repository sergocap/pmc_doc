# Weekly Payment ENG

Once a week, on Monday, weekly bonuses are calculated, funds are withdrawn to users and auto-reinvestment

* The calculation schedule file is here `config/schedule.rb`

## class `BonusesCalculateWorker`

* Bonuses are calculated `IncomeBonus`, `PassiveBonus`, `MatchingBonus`
* At the same time, time stamps for completing the calculation of bonuses in `WeeklyPayment.now` are set for control

## class `EductionAndReinvoiceWorker`

* For each user, the following is performed:
* The energy charge `user.calc_energy_payment(rate_value)` is calculated first.
* Then withdraw funds `user.eduction_funds`
* Then reinvest `user.reinvoice`
* Then, the distribution of balances in parts from mining and the program as set by the user as a percentage of `user.update_parted`
* Next is the output of the balances on `Trezor` -` Admin::TrezorService.new.eduction! `
* Statistics calculation `BtcStatistic.complete_statistic`
* And connecting inactive packages to work:
``` ruby
Invoice.not_connect_mined.each do |invoice|
  invoice.connected = true
  invoice.connected_at = Time.now
  invoice.save!
end
```

## Note

* Energy price settings $ for one TH per week can be set in `Settings.instance.energy_amount` or in the admin panel.

# Weekly Payment RUS

Раз в неделю, в понедельник происходит вычисление недельных бонусов, вывод средств юзерам и авто-реинвестирование

* Файл расписания вычислений здесь `config/schedule.rb`

## class `BonusesCalculateWorker`

* Происходит расчёт бонусов `IncomeBonus`, `PassiveBonus`, `MatchingBonus`
* Вместе с этим для контроля ставятся временные метки завершения расчёта бонусов в `WeeklyPayment.now`

## class `EductionAndReinvoiceWorker`

* Для каждого юзера выполняется следующее:
* Сначала расчитывается плата за енергию `user.calc_energy_payment(rate_value)`.
* Затем вывод средств `user.eduction_funds`
* Затем реинвестирование `user.reinvoice`
* Затем распределение остатков по частям с майнинга и программы как установил юзер в процентах `user.update_parted`
* Далее идёт вывод остатков на `Trezor` - `Admin::TrezorService.new.eduction!`
* Подсчёт статистики `BtcStatistic.complete_statistic`
* И подключение неактивных пакетов в работу:
```ruby
Invoice.not_connect_mined.each do |invoice|
  invoice.connected = true
  invoice.connected_at = Time.now
  invoice.save!
end
```

## Примечание

* Настройки цены енергии $ за один TH в неделю можно установить в `Settings.instance.energy_amount` или в админ-панели.
