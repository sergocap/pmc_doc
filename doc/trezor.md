# Trezor ENG

Using trezor it turns out to save coins safely. Every day, at a certain time, all coins from the wallet are displayed on trezor bitcoin address

## Admin Panel

![Trezor 1](img/trezor_1.png "Trezor 1")

* Here you can activate/deactivate output
* You need to click `send code`, then a pin code is sent to the mail specified in the settings to change the operating mode of the output to the trezor
* Get the code, paste it into the field and click `activate|deactivate`

## Settings

* For output to trezor to work, you need to set the following parameters in the system:
* `trezor_address, trezor_code_email` - in `config/application.yml` or `docker-compose.yml` on the server
* The output schedule can be configured in `config/schedule.rb` `TrezorEductionWorker.prepare!`

## class `Admin::TrezorService`

* In this class, the `lock/unlock` output is locked/unlocked using the `Settings.instance.trezor_code` pin code
* The output itself is `eduction!`. The output is via `BitcoinRpc` to `ENV['trezor_address']`:
```ruby
service.sendtoaddress! address: ENV ['trezor_address'], amount: balance, comment: 'trz'
```

# Trezor RU

С помощью trezor получается сохранить монеты безопасно. Каждый день, в определённое время все монеты с кошелька выводятся на trezor биткоин-адрес

## Admin Panel

![Trezor 1](img/trezor_1.png "Trezor 1")

* Здесь можно активировать/деактивировать вывод
* Нужно нажать `send code`, тогда на указаную в настройках почту отправляется пин-код для смены режима работы вывода на трезор
* Получаете код, вставляете в поле и нажимаете `activate|deactivate`

## Settings

* Для работы вывода на trezor нужно установить следующие параметры в системе:
* `trezor_address, trezor_code_email` - в `config/application.yml` или `docker-compose.yml` на сервере
* Расписание вывода можно настроить в `config/schedule.rb` `TrezorEductionWorker.prepare!`

## class `Admin::TrezorService`

* В данном классе реализована блокировка/разблокировка вывода `lock/unlock` c помощью пин-кода `Settings.instance.trezor_code`
* Сам вывод `eduction!`. Вывод происходит с помощью `BitcoinRpc` на `ENV['trezor_address']`:
```ruby
service.sendtoaddress! address: ENV['trezor_address'], amount: balance, comment: 'trz'
```
