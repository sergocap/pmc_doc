# class `BitcoinRpc` ENG

The class implements api to bitcoin rpc protocol

## Methods

* **get_new_address** - Get a new bitcoin address from a node
* **list_received_by_address** - Receive transactions at the address. minconf - lower limit of confirmations
* **list_transactions** - Get all wallet transactions
* **get_balance** - Wallet balance
* **move** - Unnecessary method. Transfer balance from account to account
* **get_transaction** - Transaction Details
* **sendtoaddress!** - Sending coins to the address
* **send_to_address** - Sending coins to an address with fixing the transaction `BitcoinTransaction` in the system
* **get_received_by_address** - Get the balance at the address

## Settings

* To work with `RPC`, you need to set these parameters in `config/application.yml` or `docker-compose.yml` on the server:
* `ENV ['BTC_RPC_USER'], ENV ['BTC_RPC_PASS'], ENV ['BTC_RPC_PORT'], ENV ['BTC_RPC_PORT'], ENV ['minimum_confirmation_for_accept']`

# class `BitcoinRpc` RU

Класс реализует апи к bitcoin rpc протоколу

## Methods

* **get_new_address** - Получает новый биткойн-адрес с ноды
* **list_received_by_address** - Получает транзакции по адресу. minconf - нижний лимит подтверждений
* **list_transactions** - Получает все транзакции кошелька
* **get_balance** - Баланс кошелька
* **move** - Ненужный метод. Перемещение баланса с аккаунта в аккаунт
* **get_transaction** - Детали транзакции
* **sendtoaddress!** - Отправка монет на адрес
* **send_to_address** - Отправка монет на адрес с фиксацией транзакции `BitcoinTransaction` в системе
* **get_received_by_address** - Получить баланс на адресе

## Settings

* Для работы с `RPC` необходимо усановить эти параметры в `config/application.yml` или `docker-compose.yml` на сервере:
* `ENV['BTC_RPC_USER'], ENV['BTC_RPC_PASS'], ENV['BTC_RPC_PORT'], ENV['BTC_RPC_PORT'], ENV['minimum_confirmation_for_accept']`
