# Rates ENG

* Quotations are taken from `https://apiv2.bitcoinaverage.com` - see `RATE_HISTORY_API_URL` in `config/application.yml` or in `docker-compose.yml`
* To quickly restore quotes, you can use `rake bitcoin:average:rates_in_year` or `rake bitcoin:average:rates_in_month`
* Quotes are cached on the system for several minutes to reduce the number of requests to `apiv2`. See `exchange_rate_update_min` in `config/application.yml` or in `docker-compose.yml`
* See `Currency` and` CurrencyRate` models

# Rates RU

* Котировки беруться из `https://apiv2.bitcoinaverage.com` - смотри `RATE_HISTORY_API_URL` в `config/application.yml` или в `docker-compose.yml`
* Для быстрого восстановления котировок можно испольковать `rake bitcoin:average:rates_in_year` или `rake bitcoin:average:rates_in_month`
* Котировки кешируются в системе на несколько минут чтобы уменьшить количество запросов к `apiv2`. Смотри `exchange_rate_update_min` в `config/application.yml` или в `docker-compose.yml`
* Смотри модели `Currency` и `CurrencyRate`
