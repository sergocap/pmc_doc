# Start with Docker ENG
                                                                                                                                                       
* Put `Docker` and `Docker compose`: [Ubuntu 17.04 x64](doc/docker.md)
* [Get it](doc/access_token.md) access to the docker registry
* Create subnet `primary`
```
docker network create --attachable -d bridge primary
```
* Make a copy of the file `docker-compose.yml.sample` in `docker-compose.yml`
* Fill the `SECRET_KEY_BASE` variable in `docker-compose.yml`
* Open access `777` to the directory `mongo/data`
* Free the ports `3000`, `9000`, `27017` and `6379`
* Raise services:
```
docker-compose up
```

# Начало с Docker RU

* Поставить `Docker` и `Docker compose`: [Ubuntu 17.04 x64](doc/docker.md)
* [Раздобыть](doc/access_token.md) доступ к docker-реестру
* Создать подсеть `primary`
```
docker network create --attachable -d bridge primary
```
* Сделать копию файла `docker-compose.yml.sample` в `docker-compose.yml`;
* Заполнить переменую `SECRET_KEY_BASE` в `docker-compose.yml`;
* Открыть доступ `777` на директорию `mongo/data`;
* Освободить порты `3000`, `9000`, `27017` и `6379`;
* Поднять сервисы:
```
docker-compose up
```
