# SavingsAccount ENG

![sa 1](img/savings_account_1.png "sa 1")
* the ability to accumulate funds from investments and from the program for reinvestment
* All user income is divided into two parts - from the program and from mining
* The user on the page `/dashboard/billing` in percent sets how much from the referral program (`reinvoice_programm_prc`) and from mining (`reinvoice_invoice_prc`)
* A request from the front goes here `ProfileController#reinvoice`
* The method `User#distribute_savings_and_eductions` saves the savings `savings_inv, savings_prog` and creates the output (funds_eduction) with the fields `funds_inv, funds_prog`

## User income

* All user income can be divided into parts:
```ruby
savings_prog + funds_prog_reinv + savings_inv + funds_inv_reinv
+ funds_eductions.ready.sum(:funds_inv) + funds_inv + funds_eductions.ready.sum(:funds_prog) + funds_prog
```

### Savings for reinvestments:
* **savings_prog** - what is accumulated from the program for reinvestments
* **funds_prog_reinv** - ready-made bonuses from the program for reinvestment, not yet formed in the withdrawal, participate in the calculations, a dynamic figure
* **savings_inv** - what is accumulated from mining for reinvestments
* **funds_inv_reinv** - ready-made bonuses from the program for reinvestment, not yet formed in the withdrawal, participate in the calculations, a dynamic figure

### Savings for output:
* `funds_eductions.ready.sum(:funds_inv)` - what is accumulated from mining for output
* `funds_eductions.ready.sum(:funds_prog)` - what is accumulated from the program for output
* **funds_inv** - ready bonuses from mining for withdrawal, not yet formed into withdrawal, participate in the calculations, a dynamic figure
* **funds_prog** - ready-made bonuses from the withdrawal program, not yet formed into the withdrawal, participate in the calculations, a dynamic figure

## Example:
* Vasya invested $ 100 and is waiting for bonuses
* Came IncomeBonus bonus of $ 10, came DirectBonus $ 15
* Vasya wants to get everything out. Fixes `reinvoice_programm_prc = 0%` and `reinvoice_invoice_prc = 0%`
* Then:
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               15$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                10$
```
* At the time of withdrawal, `funds_inv` is summed with `funds_prog` and recorded in funds_eduction
* If for some reason the withdrawal did not occur, then the withdrawal remains to wait for its implementation
* Bonuses implemented and reset
* The picture becomes the following:
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   15$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                0$
```
* Then came the IncomeBonus bonus of $ 20, came the PassiveBonus of $ 25
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   15$
funds_prog                               25$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                20$
```
* Vasya wants to reinvest everything that came from the referral program. Fixes `reinvoice_programm_prc = 100%` and `reinvoice_invoice_prc = 0%`
* Then:
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                20$
```
* Then Vasya wants to reinvest half of the mining. Fixes `reinvoice_programm_prc = 100%` and `reinvoice_invoice_prc = 50%`
* Then:
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              5$
funds_inv_reinv                          10$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    5$
funds_inv                                10$
```
* Then at the time of withdrawal ... $ 15 will be displayed (funds_inv + funds_eductions.ready.sum(:funds_inv))
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              5$
funds_inv_reinv                          10$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                0$
```
* And values for reinvestments will be recorded
```ruby
savings_prog                             40$
funds_prog_reinv                         0$
savings_inv                              15$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                0$
```
* Everything is distributed in parts as it should 100% and 50%
```ruby
savings_prog                             40$
funds_prog_reinv                         0$
savings_inv                              7.5$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    7.5$
funds_inv                                0$
```

# SavingsAccount RUS

![sa 1](img/savings_account_1.png "sa 1")
* - возможность накапливать средства от инвестиций и от программы для реинвестирования
* Все доходы пользователя разделены на две части - от программы и от майнинга
* Юзер на странице `/dashboard/billing` в процентах устанавливает сколько от реферальной программы (`reinvoice_programm_prc`) и от майнинга (`reinvoice_invoice_prc`) будет отдано на накопления
* Запрос от фронта идёт сюда `ProfileController#reinvoice`
* В методе `User#distribute_savings_and_eductions` идёт сохранение накоплений `savings_inv, savings_prog` и создания вывода (funds_eduction) c полями `funds_inv, funds_prog`

## Доход юзера

* Все доходы юзера можно разделить на части:
```ruby
savings_prog + funds_prog_reinv + savings_inv + funds_inv_reinv
+ funds_eductions.ready.sum(:funds_inv) + funds_inv + funds_eductions.ready.sum(:funds_prog) + funds_prog`
```
### Накопления для реинвестиций:
* **savings_prog** - то, что накоплено с программы для реинвестиций
* **funds_prog_reinv** - готовые бонусы с программы для реинвестирования, ещё не сформированные в вывод, участвуют в расчётах, динамичная цифра
* **savings_inv** - то, что накоплено с майнинга для реинвестиций
* **funds_inv_reinv** - готовые бонусы с программы для реинвестирования, ещё не сформированные в вывод, участвуют в расчётах, динамичная цифра

### Накопления для вывода:
* `funds_eductions.ready.sum(:funds_inv)` - то, что накоплено с майнинга для вывода
* `funds_eductions.ready.sum(:funds_prog)` - то, что накоплено с программы для вывода
* **funds_inv** - готовые бонусы с майнинга для вывода, ещё не сформированные в вывод, участвуют в расчётах, динамичная цифра
* **funds_prog** - готовые бонусы с программы для вывода, ещё не сформированные в вывод, участвуют в расчётах, динамичная цифра

## Пример:
* Вася инвестировал 100$ и ждёт бонусов
* Пришёл бонус IncomeBonus 10$, пришёл DirectBonus 15$
* Вася хочет вывести всё. Фиксирует `reinvoice_programm_prc=0%` и `reinvoice_invoice_prc=0%`
* Тогда:
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               15$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                10$
```
* В момент вывода `funds_inv` суммируется с `funds_prog` и записывается в funds_eduction
* Если по каким то причинам вывод не произошёл то вывод остаётся ждать своей реализации
* Бонусы реализованы и обнуляются
* Картина становится следующей:
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   15$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                0$
```
* Затем пришёл бонус IncomeBonus 20$, пришёл PassiveBonus 25$
```ruby
savings_prog                             0$
funds_prog_reinv                         0$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   15$
funds_prog                               25$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                20$
```
* Вася хочет реинвестировать всё то, что пришло с реферальной программы. Фиксирует `reinvoice_programm_prc=100%` и `reinvoice_invoice_prc=0%`
* Тогда:
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              0$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    10$
funds_inv                                20$
```
* Затем Вася хочет реинвестировать половину с майнинга. Фиксирует `reinvoice_programm_prc=100%` и `reinvoice_invoice_prc=50%`
* Тогда:
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              5$
funds_inv_reinv                          10$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    5$
funds_inv                                10$
```
* Затем в момент вывода ... выведется 15$ (funds_inv + funds_eductions.ready.sum(:funds_inv))
```ruby
savings_prog                             15$
funds_prog_reinv                         25$
savings_inv                              5$
funds_inv_reinv                          10$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                0$
```
* И будут записаны значения для реинвестиций
```ruby
savings_prog                             40$
funds_prog_reinv                         0$
savings_inv                              15$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    0$
funds_inv                                0$
```
* Всё распределиться по частям как надо 100% и 50%
```ruby
savings_prog                             40$
funds_prog_reinv                         0$
savings_inv                              7.5$
funds_inv_reinv                          0$
funds_eductions.ready.sum(:funds_prog)   0$
funds_prog                               0$
funds_eductions.ready.sum(:funds_inv)    7.5$
funds_inv                                0$
```
