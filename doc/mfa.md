# MFA ENG

Multi-factor authentication

## Setting

![MFA 1](img/mfa_1.png "mfa 1")
* When creating a user, it is assigned `otp_secret` - a secret key
* Then, when activated, the user is shown `otp_uri`, which he must scan with GoogleAuthenticator
* The seed is specified in the file `app/models/concerns/google_authenticator.rb` - `pmc.capital` - can be changed during initialization
* Unique code is created based on `user.email`
* See `module GoogleAuthenticator`

## Activation, deactivation

* The user activates and deactivates MFA by sending the code from `GoogleAuthenticator` to the controller `Authenticates::MfaController#mfa_active_update`

## Login

* User opens the Google Authenticator application and looks at the one-time password
* Enters into the field at login, `Authenticates::MfaController#login`
* `ROTP::TOTP` checks the correctness of the code and continues to login

# MFA RU

Multi-factor authentication

## Настройка

![MFA 1](img/mfa_1.png "mfa 1")
* При создании юзера ему присваивается `otp_secret` - секретный ключ
* Затем при активации юзеру показан `otp_uri`, который он должен отсканировать приложением GoogleAuthenticator
* Затравка указана в файле `app/models/concerns/google_authenticator.rb` - `pmc.capital` - можно менять при инициализации
* Уникальный код создаётся на основе `user.email`
* Смотри `module GoogleAuthenticator`

## Активация, отключение

* Юзер активирует и отключает MFA отправляя код из `GoogleAuthenticator` на контроллер `Authenticates::MfaController#mfa_active_update`

## Login

* Юзер открывает приложение `GoogleAuthenticator` и смотрит одноразовый пароль
* Вводит в поле при логине, `Authenticates::MfaController#login`
* `ROTP::TOTP` проверяет правильность кода и продолжает логин
