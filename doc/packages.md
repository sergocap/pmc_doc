# Packages ENG

![packages 1](img/packages_1.png "packages 1")
* After starting the application, you should create packages.
* Packages - an object worth the money
* An investment consists of one or more packages
* Each package is exchanged for a certain amount of power **powers**
* When investing, package capacities are summed up

## Creating packages on the system

* The package can be created by executing in the console:
```ruby
Package.create! (Title: 'Package 1', power: 1, price: 200)
```

## Package Power

* Each package is exchanged for a certain amount **powers** mining machines (TH/s - TeraHash per secund).
* For example, a package for 100 USD may mean a purchase of 2 TH/s
* The price for 1 TH/s is set from the admin panel at the address `/admin/set_settings`.
* When changing `Price per 1 TH/s`, package prices remain the same, but the price is recalculated - look at the` Admin :: SettingsController # update_th_capacity_amount` backing.
![packages 2](img/packages_2.png "packages 2")

# Пакеты RUS

![packages 1](img/packages_1.png "packages 1")
* После запуска приложения следует создать пакеты.
* Пакеты - объект стоящий денег
* Инвестиция состоит из одного или нескольких пакетов
* Каждый пакет обменивается на некоторое количество мощностей **powers**
* При инвестиции мощности пакетов суммируются

## Создание пакетов в системе

* Пакет можно создать выполнив в консоли:
```ruby
Package.create!(title: 'Package 1', power: 1, price: 200)
```

## Мощность пакета

* Каждый пакет обменивается на некоторое количество **powers** майнинг машин (TH/s - TeraHash per secund).
* Например пакет за 100 USD может означать покупку 2 TH/s
* Цена за 1 TH/s устанавливается с админ панели по адресу `/admin/set_settings`.
* При смене `Price per 1 TH/s` цены пакетов остаются преждними, но пересчитывается цена - смотрите на бэке `Admin::SettingsController#update_th_capacity_amount`.
![packages 2](img/packages_2.png "packages 2")
