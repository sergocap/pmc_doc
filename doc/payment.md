# Payment ENG

## Interesting fields

* **address** (String) - bitcoin address. Create when creating an entity. Obtained via BitcoinRpc#get_new_address
* **total_recieved_btc** (Float) - how many bitcoins received
* **fake_payment** (Boolean) - whether the payment is real. true by default. false on reinvestment

## Payment Process

![payments 1](img/payments_1.png "payments 1")
* User sends bitcoins to the address
* The transaction goes to the wallet and launches the callback - `bitcoind_callback_scripts/walletnotify_script.rb.example`
* The callback sends a request to the back of `BitcoindController#new_transaction_in_wallet`. The transaction is registered in the system
* For the investment, a new payment is generated with a new address if the underpayment is `Invoice#update_state!` -> `Invoice#purchase!`
* When confirming a transaction, the callback is triggered again and sends a notification to `BitcoindController#new_transaction_in_wallet`
* Depending on the number of confirmations, the status of `Payment` and` Invoice` (`Invoice#update_state!`) Changes

## Auto-renew payment

The payment is updated after a request from the front of `InvoicesController#show` and after 25 minutes of `Invoice#try_update_btc_price`

## Update blocks

* If a new block gets into the wallet, then it executes another script `bitcoind_callback_scripts/blocknotify_script.rb.example`
* It sends a request to `BitcoindController#new_block_found`
* Those transactions that were **not mistakenly written are restored** in `UpdateReceivedPaymentService.сall`
* For more information on setting up callbacks, see the documentation for setting up `bitcoind`

## Number of confirmations

The minimum number of confirmations for a transaction can be set in the file `config/application.yml` -> `minimum_confirmation_for_accept`

# Payment RU

## Интересные поля

* **address** (String) - биткоин-адрес. Создайтся при создании сущности. Получается через BitcoinRpc#get_new_address
* **total_recieved_btc** (Float) - сколько получено биткоинов
* **fake_payment** (Boolean) - настоящая ли платёжка. true по умолчанию. false при реинвестиции

## Процесс оплаты

![payments 1](img/payments_1.png "payments 1")
* Пользователь отправляет биткоины на адрес
* Транзакция попадает в кошелёк и запускает коллбэк - `bitcoind_callback_scripts/walletnotify_script.rb.example`
* Коллбек отправляет запрос на back `BitcoindController#new_transaction_in_wallet`. Транзакция регистрируется в системе
* Для инвестиции генерируется новая платёжка c новым адресом если недоплата `Invoice#update_state!` -> `Invoice#purchase!`
* При подтверждениях транзакции коллбэк срабатывает снова и отправляет уведомление на `BitcoindController#new_transaction_in_wallet`
* В зависимости от количества подтверждений меняется статус `Payment` и `Invoice` (`Invoice#update_state!`)

## Автообновление платёжки

Платёжка обновлятется после запроса с фронта `InvoicesController#show` и истечении 25 минут `Invoice#try_update_btc_price`

## Обновление блоков

* Если в кошелёк попал новый блок, то отрабатывает другой скрипт `bitcoind_callback_scripts/blocknotify_script.rb.example`
* Он отправляет запрос на `BitcoindController#new_block_found`
* Те транзакции, что были **по ошибке не записаны - восстанавливаются** в `UpdateReceivedPaymentService.сall`
* Подробенее про настройку коллбеков смотрите в документации по настройке `bitcoind`

## Количество подтверждений

Минимальное количество подтверждений для транзакции можно установить в файле `config/application.yml` -> `minimum_confirmation_for_accept`
