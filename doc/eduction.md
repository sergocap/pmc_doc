# Eduction ENG

The output occurs once a week in `EductionAndReinvoiceWorker` according to the schedule `config/schedule.rb`

## method `user.eduction_funds!`

* We take bonuses from the program and income `funds_prog` + `funds_inv`, create the output of `funds_eduction.ready!` And mark bonuses as realized `success`
* (For more information about bonus parts, see the Savings Account documentation.)
* Next, we check whether we can withdraw funds:
```ruby
def can_eduction?
  Settings.instance.project_start_time < DateTime.now && # is it time to start the project?
  email_confirmed? && # is mail verified?
  phone_verified? && # is the phone verified?
  !baned? && # is the user banned?
  !blocked_deduce? && # whether output is allowed
  !bitcoin_address.blank? # is the bitcoin address specified
end
```
* If we can not withdraw, then `funds_eductions` remain `ready`
* If we can withdraw, then we consider the withdrawal amount in USD and check the limit:
```ruby
check_sum_usd = bonus_sum_usd + overpaids_sum_usd - energy_payment_usd
                                                                                                                           
if check_sum_usd >= 10 # lower withdrawal limit 10 USD - can be changed
```
* If the condition is met, then we consider the withdrawal amount in BTC:
```ruby
final_sum_btc = bonus_sum_btc + overpaids_sum_btc - energy_payment_btc
```
* Create the final conclusion and draw the rest of the conclusions merged:
```ruby
funds_eductions.ready.update_all(_status: :merged)

final_eduction = funds_eductions.create(
  sum: check_sum_usd,
  sum_btc: final_sum_btc, # <================== amount of output
  funds_inv: final_inv,
  funds_prog: final_prog,
  funds_inv_usd: final_inv_usd,
  funds_prog_usd: final_prog_usd
)
```
* We make all overpayments and payment for energy realized - they are already aimed at the conclusion:
```ruby
final_eduction.failed! # make false to recover later on error
overpaids.update_all(_status: :success)
energy_payments.update_all(_status: :success)
```
* In the method `Payments::EductionFundsService.new(final_eduction).pay` we pay the output through `BitcoinRpc`
* Trying to print all the `funds_eductions` that ended in error:
```ruby
funds_eductions.failed.each {|failed_ed| Payments::EductionFundsService.new(failed_ed).pay }
```

# Eduction RU

Вывод происходит раз в неделю в `EductionAndReinvoiceWorker` по расписанию `config/schedule.rb`

## method `user.eduction_funds!`

* Берём бонусы с программы и доходов `funds_prog` + `funds_inv`, создаём вывод `funds_eduction.ready!` и помечаем бонусы как реализованные `success`
* (Подробнее про части бонусов можно посмотреть в документации `Savings Account`)
* Далее проверяем можем ли мы вывести средства:
```ruby
def can_eduction?
  Settings.instance.project_start_time < DateTime.now && # пришло ли время старта проекта?
  email_confirmed? && # подтверждена ли почта?
  phone_verified? && # подтверждён ли телефон?
  !baned? && # не забанен ли юзер?
  !blocked_deduce? && # разрешён ли вывод
  !bitcoin_address.blank? # указан ли биткоин-адрес
end
```
* Если не можем вывести, то `funds_eductions` остаются `ready`
* Если можем вывести, то считаем сумму вывода в USD и проверяем предел:
```ruby
check_sum_usd = bonus_sum_usd + overpaids_sum_usd - energy_payment_usd

if check_sum_usd >= 10 # нижний лимит вывода 10 USD - можно изменить
```
* Если условие выволняется, то считаем суммы вывода в BTC:
```ruby
final_sum_btc = bonus_sum_btc + overpaids_sum_btc - energy_payment_btc
```
* Создаём финальный вывод и делаем остальные выводы слитыми:
```ruby
funds_eductions.ready.update_all(_status: :merged)

final_eduction = funds_eductions.create(
  sum: check_sum_usd,
  sum_btc: final_sum_btc, # <================= сумма вывода
  funds_inv: final_inv,
  funds_prog: final_prog,
  funds_inv_usd: final_inv_usd,
  funds_prog_usd: final_prog_usd
)
```
* Все переплаты и оплату энергии делаем реализованными - они уже направлены на вывод:
```ruby
final_eduction.failed! # делаем ложной чтобы потом восстановить при ошибке
overpaids.update_all(_status: :success)
energy_payments.update_all(_status: :success)
```
* В методе `Payments::EductionFundsService.new(final_eduction).pay` оплачиваем вывод через `BitcoinRpc`
* Пытаемся вывести все `funds_eductions` которые завершились с ошибкой:
```ruby
funds_eductions.failed.each {|failed_ed| Payments::EductionFundsService.new(failed_ed).pay }
```
