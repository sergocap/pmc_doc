# Referral system EN

* The application provides a tree of users - a referral system.
* The referral system allows you to receive bonuses at the expense of invited users.
* Each user himself invites new users into the system using a referral link or sharing his username.
* Other users follow the referral link and register - this way they get one level below the sponsor from the user tree.

## User Specific Fields

Users in the system stand one under the other, forming a user tree using these fields:
* **referal_page** (String) - user referral link. The link, by clicking on which, users can register under the user creating a tree. The user copies his referral link and distributes
nyat it on the Internet.
* **username** (String) - username by which you can register by specifying in the field `sponsor`
* **referal_link_allow** (Boolean) - whether it is allowed to register under the user by his referral link.
* **referal_form_allow** (Boolean) - whether it is allowed to register as a user by specifying his sponsor username in the registration form.
* **sponsor_id** (ID) - the immediate parent of the user
* **sponsor_ids** (Array) - 11 user parents
* **network_level** (Integer) - the level of the user in the tree. For admin, this is 0. For pmc_admin - 1.
* **linear_compensation** (Integer) - the depth of the subtree. How many levels down can the user see. The maximum number is 11. The minimum is 0. It depends on the investment.

## Initial tree setup

![rs 1](img/referal_system_1.png "rs 1")
* The highest level is level 0. There can only be one user at this level - the administrator. Created manually during application initialization. For example:
``` ruby
User.create! ({
  email: 'admin@gmail.com',
  phone_number: "89999999999",
  username: 'admin',
  role:: admin,
  password: 'ADMIN123123admin',
  network_level: 0,
  sponsor_id: nil
})
```
* Then - level 1. At this level there can be only one user. It can be registered via the interface by specifying `sponsor = admin` and` username = pmc_admin`.
* Next, you need to execute the code that prohibits registration under the administrator. **Thus, at level 0 there will be only one user and at level 1 there will be only one user.**
```sh
$ rake users: setup_blocks
```
* Or in rails console
```ruby
pmc = User.where(username: 'pmc_admin').first
admin = User.where(_role: :admin).first

User.update_all(referal_link_allow: true, referal_form_allow: true)

pmc.update_attributes(referal_link_allow: true, referal_form_allow: false)
admin.update_attributes(referal_link_allow: false, referal_form_allow: false)
```

## Bonus accrual

* Depending on the attachment, the user sees a different number of users down.
* For example, the user invested $ 100. Then `linear_compensation` becomes 2 and **the subtree** of the user consists of two levels below it.
* Due to this subtree different bonuses are calculated.
* The more attachments, the more `linear_compensation`, the more subtree, the more bonuses.

## Note

* username pmc_admin can be changed in the code - see User#is_pmc_agent?

# Реферальная система RUS

* В приложении предусмотрено дерево пользователей - реферальная система.
* Реферальная система позволяет получать бонусы за счёт приглашённых юзеров.
* Каждый пользователь сам приглашает в систему новых юзеров с помощью реферальной ссылки или поделившись своим username.
* Другие пользователи переходят по реферальной ссылки и регистрируются - таким образом встают на уровень ниже спонсора с дереве пользователей.

## Специфичные поля User

Пользователи в системе выстаиваются один под другим, образуя дерево пользователей c помощью данных полей:

* **referal_page** (String) - реферальная ссылка пользователя. Ссылка, перейдя по которой, юзеры могут регистрироваться под юзером создавая дерево. Пользователь копирует свою реферальную ссылку и распространяет её в интернете.
* **username** (String) - имя пользователя по которому можно регистрироваться указав в поле `sponsor`
* **referal_link_allow** (Boolean) - разрешено ли регистрироваться под пользователем по его реферальной ссылке.
* **referal_form_allow** (Boolean) - разрешено ли регистрироваться под пользователем указав его sponsor username в форме при регистрации.
* **sponsor_id** (ID) - непосредственный родитель юзера
* **sponsor_ids** (Array) - 11 родителей юзера
* **network_level** (Integer) - уровень нахождения юзера в дереве. Для админа это 0. Для pmc_admin - 1.
* **linear_compensation** (Integer) - глубина поддерева. Cколько уровней вниз может видеть юзер. Максимальное количество 11. Минимальное 0. Зависит от вложений.

## Первоначальная настройка дерева

![rs 1](img/referal_system_1.png "rs 1")
* Самый верхний уровень - 0 уровень. На этом уровне может быть только один пользователь - администратор. Создаётся вручную при инициализации приложения. Например:

```ruby
User.create!({
  email: 'admin@gmail.com',
  phone_number: "89999999999",
  username: 'admin',
  role: :admin,
  password: 'ADMIN123123admin',
  network_level: 0,
  sponsor_id: nil
})
```

* Затем - уровень 1. На этом уровне может быть только один пользователь. Его можно зарегистрировать через интерфейс указав `sponsor = admin` и `username = pmc_admin`.
* Далее нужно выполнить код, который запрещает регистрироваться под админом. **Такие образом, на уровне 0 будет только один пользователь и на уровне 1 будет только один юзер.**

```sh
$ rake users:setup_blocks
```

* Или в rails console

```ruby
pmc = User.where(username: 'pmc_admin').first
admin = User.where(_role: :admin).first

User.update_all(referal_link_allow: true, referal_form_allow: true)

pmc.update_attributes(referal_link_allow: true, referal_form_allow: false)
admin.update_attributes(referal_link_allow: false, referal_form_allow: false)
```

## Начисление бонусов

* В зависимости от вложений пользователь видит разное количество юзеров вниз.
* Например юзер вложил 100$. Тогда `linear_compensation` становиться 2 и **поддерево** пользователя состоит из двух уровней под ним.
* За счёт этого поддерева вычисляются разные бонусы.
* Чем больше вложений, тем больше `linear_compensation`, тем больше поддерево, тем больше бонусов.

## Примечание

* username pmc_admin может быть изменён в коде - смотрите User#is_pmc_agent?
