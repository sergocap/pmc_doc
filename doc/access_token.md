# Create `Access Token` to access the docker registry: ENG

* Go to your profile settings on [gitlab.com](https://gitlab.com/profile)
* Go to the [Access Tokens](https://gitlab.com/profile/personal_access_tokens)
* Indicate `name`
* Below, in the `Scopes` put 2 checkmarks: `api` and `read_registry`
* Click the button to create `Access Token`

### Authorization in the system (Ubuntu)

```
docker login registry.gitlab.com
```
> Username - `name` specified when creating `Access Token`
> Password - the `token` itself

# Создать `Access Token` для доступа к docker-реестру: RU

* Зайти в настройки своего профиля на [gitlab.com](https://gitlab.com/profile);
* Перейти в раздел [Access Tokens](https://gitlab.com/profile/personal_access_tokens);
* Указать `имя`;
* Ниже, в `Scopes` поставить 2 галочки: `api` и `read_registry`;
* Нажать кнопку создать `Access Token`;

### Авторизация в системе (Ubuntu)

```
docker login registry.gitlab.com
```
> Username - `имя`, указанное при создании `Access Token`
> Password - сам `токен`
