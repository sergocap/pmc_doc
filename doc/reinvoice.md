# Reinvoice ENG

Reinvestment is an `Invoice` with the flag `Reinvoice = true`. Re-investment occurs once a week in `EductionAndReinvoiceWorker` according to the schedule `config/schedule.rb`

## method `user.reinvoice!`

* We take parts of bonuses for reinvestment and accumulation:
* (For more information about bonus parts, see the Savings Account documentation.)
```ruby
reinvoice_sum_btc = funds_prog_reinv + funds_inv_reinv + savings_total
```
* Next, see what packages we can buy:
``` ruby
Package.order(price: :desc).each do |pack|
  count = (reinvoice_sum / pack.price).to_i
  if count > 0
    reinvoice_params << {package: pack, count: count}
    reinvoice_sum %= pack.price
  end
end
```
* Next, we try to make an investment and save
* If all is well, then we pay the investment and save the balance
``` ruby
Payments::AutoInvestService.new(payment).pay
update_attributes :savings_inv => pie, :savings_prog => pie
```
* If investment is not possible, then we send parts of bonuses to accumulations ... and we make bonuses reinvested (they are already in accumulations)
``` ruby
update_attributes :savings_inv => savings_inv + fir, :savings_prog => savings_prog + fpr
reinvested_all_bonuses!
```

# Reinvoice RU

Реинвестиция - это `Invoice` с флагом `Reinvoice=true`. Реинвестиция происходит раз в неделю в `EductionAndReinvoiceWorker` по расписанию `config/schedule.rb`

## method `user.reinvoice!`

* Берём части бонусов для реинвестиции и накопления:
* (Подробнее про части бонусов можно посмотреть в документации `Savings Account`)
```ruby
reinvoice_sum_btc = funds_prog_reinv + funds_inv_reinv + savings_total
```
* Далее смотрим какие пакеты можем купить:
```ruby
Package.order(price: :desc).each do |pack|
  count = (reinvoice_sum / pack.price).to_i
  if count > 0
    reinvoice_params << { package: pack, count: count }
    reinvoice_sum %= pack.price
  end
end
```
* Далее пытаемся сделать инвестицию и сохранить
* Если всё хорошо, то проводим оплату инвестиции и сохраняем остаток
```ruby
Payments::AutoInvestService.new(payment).pay
update_attributes :savings_inv => pie, :savings_prog => pie
```
* Если инвестиция невозможна, то отправляем части бонусов в накопления ... и делаем бонусы реинвестированными (они уже в накоплениях)
```ruby
update_attributes :savings_inv => savings_inv + fir, :savings_prog => savings_prog + fpr
reinvested_all_bonuses!
```
