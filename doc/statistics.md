# Statistics ENG

* Statistics on the site are presented by three models `Statistic, BtcStatistic (Treasure), PersonalStatistic`
* You can set the statistics calculation schedule in the file `config/schedule.rb` - `StatisticsWorker.prepare!, TreasureFreshWorker.prepare!, PersonalStatisticsWorker.prepare! `

## class `Statistic`

![Statistic 1](img/statistic_1.png "statistic 1")
* Chart display can be found in the admin panel
* Stores general information on user activity, bonuses, accruals, conclusions
* Calculation in `Admin::StatisticService`

## class `BtcStatistic`

![Statistic 2](img/statistic_2.png "statistic 2")
* Chart display can be found in the admin panel
* Stores detailed cash flow information
* Calculation in `BtcStatistic.fresh_now!`

## class `PersonalStatistic`

![Statistic 3](img/statistic_3.png "statistic 3")
* Chart display can be found in user dashboard
* Stores user subtree information
* Calculation in `PersonalStatisticService`

# Statistics RU

* Статистика на сайте представлена тремя моделями `Statistic, BtcStatistic (Treasure), PersonalStatistic`
* Настроить расписание расчёта статистики можно в файле `config/schedule.rb` - `StatisticsWorker.prepare!, TreasureFreshWorker.prepare!, PersonalStatisticsWorker.prepare!`

## class `Statistic`

![Statistic 1](img/statistic_1.png "statistic 1")
* Отображение графиков можно найти в админ панели
* Хранит общую информацию по активности юзеров, бонусам, начислениям, выводам
* Расчёт в `Admin::StatisticService`

## class `BtcStatistic`

![Statistic 2](img/statistic_2.png "statistic 2")
* Отображение графиков можно найти в админ панели
* Хранит подробную информацию о движении средств
* Расчёт в `BtcStatistic.fresh_now!`

## class `PersonalStatistic`

![Statistic 3](img/statistic_3.png "statistic 3")
* Отображение графиков можно найти в dashboard пользователя
* Хранит информацию о поддереве пользователя
* Расчёт в `PersonalStatisticService`
