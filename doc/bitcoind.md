# Bitcoind ENG

## Installation

* Installation instructions can be found [here](https://bitcoin.org/en/full-node#linux-instructions)
* Example bitcoin.conf
```bash
server=1
testnet=1
daemon=1
prune=550
listen=1
rpcthreads=4

rpcpassword=tesnet_pass
rpcuser=bitcoin_rpc_manager
rpcallowip=0.0.0.0/0
rpcallowip=::/0

blocknotify=/home/deploy/.bitcoin/blocknotify_script.rb % s
walletnotify=/home/deploy/.bitcoin/walletnotify_script.rb % s
```

## Callbacks

* To receive notifications about transactions and about blocks, you need to set the `blocknotify` and `walletnotify` parameters in `bitcoin.conf`
* To do this, put `ruby` (2.5.1) on the server and `gems`:
```bash
gem install net-http-digest_auth
```
* Examples of callbacks are in `bitcoind_callback_scripts`. They need to be put on the server and configured:
```ruby
logfile_path
tx_data_full['confirmations'] > 6 # can be changed
uri # url hook api
uri.user # user to authorize the request. see ENV bitcoind_api_user
uri.password # password for request authorization - ENV bitcoind_api_pass
host_header
```

# Bitcoind RU

## Установка

* Инструкцию по установке можно найти [здесь](https://bitcoin.org/en/full-node#linux-instructions)
* Пример bitcoin.conf
```
server=1
testnet=1
daemon=1
prune=550
listen=1
rpcthreads=4

rpcpassword=tesnet_pass
rpcuser=bitcoin_rpc_manager
rpcallowip=0.0.0.0/0
rpcallowip=::/0

blocknotify=/home/deploy/.bitcoin/blocknotify_script.rb %s
walletnotify=/home/deploy/.bitcoin/walletnotify_script.rb %s
```
## Коллбеки

* Для получения уведомлений о транзакциях и о блоках необходимо установить параметры `blocknotify` и `walletnotify` в `bitcoin.conf`
* Для этого нужно поставить `ruby` (2.5.1) на сервер и `gems`:
```bash
gem install net-http-digest_auth
```
* Примеры коллбеков лежат в `bitcoind_callback_scripts`. Их нужно положить на сервер и настроить:
```ruby
logfile_path
tx_data_full['confirmations'] > 6 # можно изменить
uri # url хука апи
uri.user # user для авторизации запроса. смотри ENV bitcoind_api_user
uri.password # password для авторизации запроса - ENV bitcoind_api_pass
host_header
```
