# Bonuses ENG

**Sponsor** - user, **referral** - user under the sponsor in the tree

* IncomeBonus - income from mining - direct mining, depends on own investments - weekly
* PassiveBonus - bonus on direct subtree income - weekly
* CareerBonus - career income from investments of subtree teams, accrued upon reaching a certain limit - one-time
* DirectBonus - a linear bonus, accrued immediately when investing a invited user - instant
* MatchingBonus - Matching, depends on DirectBonus subtree and status - weekly

# IncomeBonus

* Charges will be made once a week, when money for mining will come from the pool
* Take the total capacity for a week
* The user takes the sum of investment capacities that are in work
* Take the percentage of the user of the total capacity and determine the percentage of mined
* See `Bonuses::IncomeBonusService`
* Example:

``` ruby
bv_btc_power = 6.4
user1_power = 1.6
all_btc_mined = 4.342
user_procent = user1_power / bv_btc_power (0.25)
profit = all_btc_mined * user_procent (1.0855)
```

# PassiveBonus

* Accrued once a week after the mining bonus `IncomeBonus`
* The `IncomeBonus` subtree is taken
* Paid `7%, 10.5%, 14%` of the amount depending on the status of the user - `investor, partner, vip`
* See `Bonuses::PassiveBonusService`
* Example:
``` ruby
sum_income_bonus_btc = 10.0
user1_status = 'investor'
profit = 0.07 * sum_income_bonus_btc => 0.7
```

# DirectBonus

* When the referral pays for the package, then payment to the sponsor should be made immediately
* Depending on the level of the sponsor, a percentage of the investment is taken over the referral
* See `Bonuses::DirectBonusService`
* Calculation table (1st level - direct sponsor, 2nd level - sponsor sponsor):

| Level         | %                  |
| ------------- |:------------------:|
| 1             |       10%          |
| 2							|       3%           |
| 3							|       3%           |
| 4							|       2%           |
| 5							|       2%           |
| 6							|       1%           |
| 7							|       1%           |
| 8							|       1%           |
| 9							|       1%           |
| 10 						|       0.5%         |
| 11 						|       0.5%         |

# CareerBonus

* It is paid once, and the user is assigned a career status in the company for the creation of a group, accumulative turnover
* Creation of a group accumulative turnover for a total amount of $ 25,000 or more (see table)
* One partner of the structure cannot bring in turnover more than 50% of the required sales volume i.e. $ 12,500, that is, to close the Miner status of $ 25,000, you need at least 2 different
Tags that made 50% or 1 branch + the sum of all other branches
* See `Bonuses::CareerBonusService`
* Calculation table:

| Status        | Turnover           | Premia				  	 |
| ------------- |:------------------:|:-----------------:|
| Miner         |      25.000$       |      500$         |
| Steel         |      50.000$       |      1000$        |
| Bronze        |      100.000$      |      2500$        |
| Silver        |      500.000$      |      10000$       |
| Gold          |      1.000.000$    |      25000$       |
| Platinum      |      3.000.000$    |      75.000$      |
| Diamond       |      10.000.000$   |      250.000$     |
| Co-founder    |      30.000.000$   |      350.000$     |

# MatchingBonus

* Accrued and paid once a week
* See `Bonuses::MatchingBonusService`
* Remuneration from the amount of income on linear reward `DirectBonus` 1,2,3 lines
* Calculation table:

| 1 level(Partner, VIP)  |2 level(VIP)        |3 level(VIP)       |
| ---------------------- |:------------------:|:-----------------:|
| 10%                    |         5%         |         5%        |

# Bonuses RU

**Спонсор** - юзер, **реферал** - юзер под спонсором в дереве

* IncomeBonus - доход от майнинга - непосредственная добыча, зависит от собственных вложений - недельный
* PassiveBonus - бонус от непосредственного дохода поддерева - недельный
* CareerBonus - карьерный доход от вложений команд поддерева, начисляется при достижении определённого лимита - разовый
* DirectBonus - линейный бонус, начисляется сразу при инвестировании приглашённого юзера - моментальный
* MatchingBonus - мэтчинг, зависит от DirectBonus поддерева и статуса - недельный

# IncomeBonus

* Начисление будут производиться раз в неделю, когда из пулла будут приходить деньги за майнинг
* Береться общая мощность обьема за неделю
* У пользователя беретсья сумма мощностей инвестиции которые в работе
* Береться процент пользователя от общего объема мощности и определяеться процент с добытого
* Смотри `Bonuses::IncomeBonusService`
* Пример:

```ruby
bv_btc_power = 6.4
user1_power = 1.6
all_btc_mined = 4.342
user_procent = user1_power / bv_btc_power (0.25)
profit = all_btc_mined * user_procent (1.0855)
```

# PassiveBonus

* Начисляеться раз в неделю после бонуса майнинга `IncomeBonus`
* Берётся `IncomeBonus` поддерева
* Выплачиваеться `7%, 10.5%, 14%` суммы в зависимости от статуса юзера - `investor, partner, vip`
* Смотри `Bonuses::PassiveBonusService`
* Пример:

```ruby
sum_income_bonus_btc = 10.0
user1_status = 'investor'
profit = 0.07 * sum_income_bonus_btc => 0.7
```

# DirectBonus

* Когда реферал оплачивает пакет, то моментально должна производиться оплата спонсору
* В зависимости от уровня спонсора над рефералом берётся процент от инвестиции
* Смотри `Bonuses::DirectBonusService`
* Таблица рассчета (1 уровень - непосредстенный спонсор, 2 уровень - спонсор спонсора):

| Уровень       | Процент            |
| ------------- |:------------------:|
| 1 уровень     |       10%          |
| 2 уровень     |       3%           |
| 3 уровень     |       3%           |
| 4 уровень     |       2%           |
| 5 уровень     |       2%           |
| 6 уровень     |       1%           |
| 7 уровень     |       1%           |
| 8 уровень     |       1%           |
| 9 уровень     |       1%           |
| 10 уровень    |       0.5%         |
| 11 уровень    |       0.5%         |

# CareerBonus

* Выплачивается единоразово, а также пользователю присваивается карьерный статус в компании за создание группового, накопительного товарооборота
* Создание группового накопительного оборота на общую сумму от 25.000$ и более (см.таблицу)
* Один партнер структуры не может принести в оборот более чем 50% от нужного объема продаж т.е. 12 500 долларов, то есть для закрытия статуса Miner в 25.000$ нужно, чтобы в структуре было минимум 2 разные ветки которые сделали по 50% или 1 ветка + сумма всех остальных веток
* Смотри `Bonuses::CareerBonusService`
* Таблица рассчета:

| Статус        |Объем для выполнения|Единоразовая премия|
| ------------- |:------------------:|:-----------------:|
| Miner         |      25.000$       |      500$         |
| Steel         |      50.000$       |      1000$        |
| Bronze        |      100.000$      |      2500$        |
| Silver        |      500.000$      |      10000$       |
| Gold          |      1.000.000$    |      25000$       |
| Platinum      |      3.000.000$    |      75.000$      |
| Diamond       |      10.000.000$   |      250.000$     |
| Co-founder    |      30.000.000$   |      350.000$     |

# MatchingBonus

* Начисляеться и выплачиваеться раз в неделю
* Смотри `Bonuses::MatchingBonusService`
* Вознограждение от суммы доходов по линейным вознаграждением `DirectBonus` 1,2,3 линии
* Таблица рассчета:

| 1 линия(Partner, VIP)  |2 линия(VIP)        |3 линия(VIP)       |
| ---------------------- |:------------------:|:-----------------:|
| 10%                    |         5%         |         5%        |
